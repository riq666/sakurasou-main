import React from "react";
import { Container } from "react-bootstrap";
import Particle from "../Particle";
import Techstack from "./Techstack";
import Toolstack from "./Toolstack";

function About() {
  return (
    <Container fluid className="about-section">
      <Particle />
      <Container>
        <h1 className="project-heading">
          These are technologies that we used to <strong className="purple">Build this website </strong>
        </h1>

        <Techstack />
        <Toolstack />
      </Container>
    </Container>
  );
}

export default About;
