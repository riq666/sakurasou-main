import React from "react";
import Typewriter from "typewriter-effect";

function Type() {
  return (
    <Typewriter
      options={{
        strings: [
          "Membahas segala hal dari a sampai z",
          "Kadang membahas linux",
          "isinya rata2 anak smk",
          "Kadang membahas koding",
          "Ownernya sangat ramah",
        ],
        autoStart: true,
        loop: true,
        deleteSpeed: 50,
      }}
    />
  );
}

export default Type;
