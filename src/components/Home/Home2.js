import React from "react";
import { Container, Row, Col } from "react-bootstrap";
import myImg from "../../Assets/avatar.svg";
import Tilt from "react-parallax-tilt";

function Home2() {
  return (
    <Container fluid className="home-about-section" id="about">
      <Container>
        <Row>
          <Col md={8} className="home-about-description">
            <h1 style={{ fontSize: "2.6em" }}>
              LET US <span className="purple"> INTRODUCE </span> OUR DISCORD SERVER
            </h1>
            <p className="home-about-body"> 
              Sakurasou Perjoeangan is a  
              <br />
              <i>
                <b className="purple"> Discord server </b>
              </i>
              <br />
              <br />
              that is dedicated to the Japanese anime community. &nbsp;
              <i>
                <b className="purple">We are a group of people </b> who love anime and anime-related topics.
              </i>
              <br />
              <br />
              yes, we are anime fans. <b className="purple">- Github Copilot</b>
            </p>
          </Col>
          <Col md={4} className="myAvtar">
            <Tilt>
              <img src={myImg} className="img-fluid" alt="avatar" />
            </Tilt>
          </Col>
        </Row>
      </Container>
    </Container>
  );
}
export default Home2;
