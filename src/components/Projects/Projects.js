import React from "react";
import { Container, Row, Col } from "react-bootstrap";
import ProjectCard from "./ProjectCards";
import Particle from "../Particle";

import wojak from "../../Assets/Projects/wojak.jpg";

function Projects() {
  return (
    <Container fluid className="project-section">
      <Particle />
      <Container>
        <h1 className="project-heading">
          Our <strong className="purple">Members </strong>
        </h1>
        <p style={{ color: "white" }}>
          Here are some of our members who are active in our server.
        </p>
        <Row style={{ justifyContent: "center", paddingBottom: "10px" }}>
          <Col md={4} className="project-card">
            <ProjectCard
              imgPath={wojak}
              isBlog={false}
              title="Dicky"
              description="Owner"
            />
          </Col>

          <Col md={4} className="project-card">
            <ProjectCard
              imgPath={wojak}
              isBlog={false}
              title="placeholder"
              description=""
            />
          </Col>

          <Col md={4} className="project-card">
            <ProjectCard
              imgPath={wojak}
              isBlog={false}
              title="placeholder"
              description=""
            />
          </Col>

          <Col md={4} className="project-card">
            <ProjectCard
              imgPath={wojak}
              isBlog={false}
              title="placeholder"
              description=""
            />
          </Col>

          <Col md={4} className="project-card">
            <ProjectCard
              imgPath={wojak}
              isBlog={false}
              title="placeholder"
              description=""
            />
          </Col>

          <Col md={4} className="project-card">
            <ProjectCard
              imgPath={wojak}
              isBlog={false}
              title="placeholder"
              description=""
            />
          </Col>
        </Row>
      </Container>
    </Container>
  );
}

export default Projects;
